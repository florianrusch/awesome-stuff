# macOS

## Resize a .dmg image file

I've tried to resize an filled up, encrypted `.dmg` image file with the **Disk Utility** but got then the message, that the image very not available or it can't be resized. As I read at a [blog post](https://eclecticlight.co/2019/03/16/disk-utility-cant-resize-apfs-disk-images-but-hdiutil-can/) it's depending to the used filesystem APFS which seems to be not complete supportet by the **Disk Utility** tool. As the author of the blog post descriptes we have to use the command line to resize the image and after that the container in it. We are using for that two tools: `hdiutil` and `diskutil`.

With the `hdiutil` cli you can do some cool things with disk images right from you command line (taken from [here](https://commandlinemac.blogspot.com/2008/12/using-hdiutil.html)):

- Creating a disk image from a folder
- Mounting a disk image
- Unmounting a disk image
- Burning an ISO to CD (or DVD)
- Create an encrypted disk image
- and so one

And with `diskutil` you can perform some disk operations like modify, verify and repair.

To resize the image I've done the following things:

```shell
# Resize the image itself
$ hdiutil resize -growonly -size 5g my-image.dmg
Enter password to access "my-image.dmg": ***

# Mount the image and grep the dev device name for the APFS container – in this case it's disk2s1
$ hdiutil mount sqldata.dmg
Enter password to access "my-image.dmg":
/dev/disk2          	GUID_partition_scheme
/dev/disk2s1        	Apple_APFS
/dev/disk3          	EF57347C-0000-11AA-AA11-0030654
/dev/disk3s1        	41504653-0000-11AA-AA11-0030654	/Volumes/my-image

# Resize the container
$ diskutil apfs resizeContainer disk2s1 0
Started APFS operation
Aligning grow delta to 4.085.858.304 bytes and targeting a new physical store size of 5.368.668.160 bytes
Determined the maximum size for the targeted physical store of this APFS Container to be 5.367.640.064 bytes
Resizing APFS Container designated by APFS Container Reference disk3
The specific APFS Physical Store being resized is disk2s1
Verifying storage system
Performing fsck_apfs -n -x -S /dev/disk2s1
Checking the container superblock
Checking the space manager
Checking the space manager free queue trees
Checking the object map
Checking volume
Checking the APFS volume superblock
The volume my-image was formatted by newfs_apfs (945.275.7) and last modified by apfs_kext (945.275.9)
Checking the object map
Checking the snapshot metadata tree
Checking the snapshot metadata
Checking the extent ref tree
Checking the fsroot tree
Verifying allocated space
The volume /dev/disk2s1 appears to be OK
Storage system check exit code is 0
Growing APFS Physical Store disk2s1 from 1.282.809.856 to 5.368.668.160 bytes
Modifying partition map
Growing APFS data structures
Finished APFS operation
```

To validate the resize you can take a view into the **Disk Utility** while the image is mounted.
