# Onboarding Guide

Basic Info every new joiner would need:

* Permissions
* Where I downloaded certain tools
* How I set up my dev environment
* Weird issues I ran into
* etc

Common procedures:

* How do you check the production logs?
* How do you request a new DB?
* How do you request a new service account?
* How do you deploy this app?
* etc

Information about the system I work on:

* How does this system work?
* What other system does it talk to?
* What are common terms we use
* etc
