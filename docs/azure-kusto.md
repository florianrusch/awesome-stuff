# Azure Kusto Queries

## Tipps and Shortcuts

### DateTime filtering

```kusto
| where TimeGenerated between(datetime(2021-01-27 13:00:00Z) .. datetime(2021-01-28 12:45:00Z))
```

## Azure Kuberentes Service

### Container Logs for a specific service

```kusto
KubePodInventory
| where Name contains "xxx" // Pod-Name
| where TimeGenerated between(datetime(2021-01-27 13:00:00Z) .. datetime(2021-01-28 12:45:00Z))
| project ContainerID, Name
| join (
    ContainerLog
    | where TimeGenerated between(datetime(2021-01-27 13:00:00Z) .. datetime(2021-01-28 12:45:00Z))
    | project TimeGenerated, LogEntry, ContainerID
) on ContainerID
| sort by TimeGenerated asc
| project TimeGenerated, Name, LogEntry
```

### Amount of log entries per pod

> ⚠️ Be careful with the time range, because depending on your setup, there could be millions of log entries!

```kusto
KubePodInventory
| where TimeGenerated > startofday(ago(3h))
| project ContainerID, Name
| join (
    ContainerLog
    | where TimeGenerated  > startofday(ago(3h))
    | project TimeGenerated, LogEntry, ContainerID
) on ContainerID
| summarize count() by bin(TimeGenerated, 3h), Name
```
