# Awesome-Links

## Naming conventions

| Name | Example |
|---|---|
| Snake Case | `hallo_are_you_there` | 
| All Caps/Screaming Snake Case | `HALLO_ARE_YOU_THERE` |
| Kebab Case | `hallo-are-you-there` |
| All Caps/Screaming Kebab Case | `HALLO-ARE-YOU-THERE` |
| Camel Case | `halloAreYouThere` |
| Pascal Case (or Upper Camel Case) | `HalloAreYouThere` |

## Knowledge sharing and management

- [Logseq](https://logseq.com/) [Accesssed 2021-01-23]
  - Privacy-first, open-source platform.
  - Like Notion but just with privacy and open-source.

## Webdeveloping

- [Favicon Generator](https://realfavicongenerator.net/) [Accessed 2019-05-10]

## User Experience

- [What you should know about Skeletons (Study)](https://uxdesign.cc/what-you-should-know-about-skeleton-screens-a820c45a571a) [Accessed 2021-07-20]

## MacOS

- [Spaces in Dock](https://apple.stackexchange.com/questions/327325/can-i-add-a-space-to-my-dock-to-separate-apps-into-groups) [Accessed 2019-06-30]

## Interesting 404 Pages

- [Sipgate APP](https://app.sipgatebasic.de/404) [Accessed 2019-01-12]

## YAML

- [Anchors, References, Extend](https://blog.daemonl.com/2016/02/yaml.html) [Accessed 2019-01-02]
- [Achore Example](https://gist.github.com/bowsersenior/979804) [Accessed 2019-01-02]

## Docker

- [Tips, Tricks and Tutorials](https://nickjanetakis.com/blog/tag/docker-tips-tricks-and-tutorials) [Accessed 2019-01-02]

## Kubernetes

- [stakater/Reloader](https://github.com/stakater/reloader) [Accessed 2019-04-14]


### CNI

- [Writing own CNI Plugin](https://www.altoros.com/blog/kubernetes-networking-writing-your-own-simple-cni-plug-in-with-bash/) [Accessed 2019-01-01]

### Services

- [`type: ExternalName`](https://kubernetes.io/docs/concepts/services-networking/service/#externalname) [Accessed 2018-12-18]

### Production

- [Automatic https with lets-encrypt](https://akomljen.com/get-automatic-https-with-lets-encrypt-and-kubernetes-ingress/) [Accessed 2018-11-28]

### Anwendungen
#### Security

- [AppsCode Guard - Authentication WebHook Server](https://appscode.com/products/guard/) [Accessed 2019-01-21]

## GitLab

### CI/CD

- [CI YAML - `only:changes` and `except:changes`](https://docs.gitlab.com/ee/ci/yaml/#onlychanges-and-exceptchanges) [Accessed 2018-12-18]

## Wissenschaftliches Arbeiten

### Zitation

- [Zitate in wissenschaftlichen_Arbeite - TU-Braunschweig](http://www.ibmb.tu-braunschweig.de/tl_files/ibmb/baustoffe/Studentische_Arbeiten/Zitate_in_wissenschaftlichen_Arbeiten.pdf)  [Accessed 2018-12-04]
