# Azure DevOps

## Pipelines

### Shell/Bash commands

<https://github.com/microsoft/azure-pipelines-tasks/blob/master/docs/authoring/commands.md>


```shell
echo "##vso[task.setvariable variable=testvar]testvalue"
echo "##vso[task.setvariable variable=testvar;issecret=true]testvalue"
```