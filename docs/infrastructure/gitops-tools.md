# GitOps Tools

> ⚠️ This could be outdated! ⚠️
>
> Last update: Mar 14, 2019

## [Weave Flux](https://github.com/weaveworks/flux)

- One Flux Controller per repo
    - One repo could be scanned from multiple flux controllern
    - It's possible to handle multiple envs in one repo ([FAQ](https://github.com/weaveworks/flux/blob/master/site/faq.md#i-have-a-dedicated-kubernetes-cluster-per-environment-and-i-want-to-use-the-same-git-repo-for-all-how-can-i-do-that))
    - Removing resources are still an experimental feature which has to be enabled manually
    - Helm Charts will not be removed if the `HelmRelease` CRD is deleted from the repo. The `HelmRelease` CRD-Object has be removed manually in the cluster. ([FAQ](https://github.com/weaveworks/flux/blob/master/site/faq.md#ive-deleted-a-helmrelease-file-from-git-why-is-the-helm-release-still-running-on-my-cluster))
- Can be extended via operators (e.g. Helm Operator)
    - Is working with real Helm Releases
- Use CRD for defining a `HelmRelease`
- Docker Registry scanning for new Images
    - Scanning via Regex (e.g. `master-*`, `dev-*`)
- Namespace whitelisting is possible ([FAQ](https://github.com/weaveworks/flux/blob/master/site/faq.md#can-i-restrict-the-namespaces-that-flux-can-see-or-operate-on))
- Events could be thrown to slack and webhooks ([GitHub Repo](https://github.com/justinbarrick/fluxcloud))
- `Flux` Tag in Repository to mark the latest deployed commit
    - **NEGATIVE** Needs write access on the repo
    - No sync back from cluster to repo
- Integrates with Weave Cloud
- [Flux Operator](https://github.com/justinbarrick/flux-operator) to easily install multiple Flux instances

## [argo](https://argoproj.github.io/argo-cd/)

- Running as a k8s controller
- Able to deploy to multiple clusters
- Automated or manual syncing of applications to its desired state
- Web UI which provides real-time view of application activity
- CLI for automation and CI integration
- Webhook integration
- Prometheus metrics
- SSO Integration
- **NEGATIVE** deployes helm charts as normal k8s resources
    - -> No helm rollback mechanism or other helm benefits
- **NEGATIVE** deployment via helm chart possible but not very well documented

## [autoapply](https://github.com/autoapply/autoapply)

- Deploys K8s-Resourcen and Helm Charts
- Simply pull repo and run `kubectl apply` or what every you want
- Runs in the cluster

## GitLab mit der K8s Integration

- **NEGATIVE** Needs own installation of Tiller
- **NEGATIVE** The Prometheus installation doesn't include the Metric-Server
- No really benefits

## [Ship](https://github.com/replicatedhq/ship)

- Deploys Helm Charts
- Needs for every Helm Chart an seperated process
- No centered instance -- is living in the ci pipeline
