# Helm snippets

## Variable rendering in `values.yaml`

[Source](https://github.com/helm/charts/blob/0445fad8d8308f089f5fb6756a5570bc6d6f0bf5/stable/keycloak/templates/statefulset.yaml#L67)

```yaml
{{- with .Values.keycloak.extraEnv }}
{{ tpl . $ | indent 12 }}
{{- end }}
```

So it treats extraEnv as a string, sends it through the tpl function so that it is evaluated as part of the template and ensures that the correct indenting is applied. This is interesting as it allows values to be set like:

```yaml
extraEnv: |
 — name: KEYCLOAK_LOGLEVEL
   value: DEBUG
 — name: HOSTNAME
   value: {{ .Release.Name }}-keycloak
```

## Escape `{}` in helm

```yaml
foobar: {{`In this string are some {} which shouldn't interpreted via helm.`}}
```

or

```yaml
foobar: {{ printf "In this string are some {} which shouldn't interpreted via helm." }}
```
