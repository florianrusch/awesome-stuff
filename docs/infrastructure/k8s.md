# Kubernetes

## Context and Namespaces

### Show current/all context

```shell
$ kubectl config get-contexts
CURRENT   NAME             CLUSTER          AUTHINFO                        NAMESPACE
          dev              dev_cluster      dev_user                        default
          minikube         minikube         minikube                        default
*         prod             prod_cluster     production_user                 default
          staging          staging_cluster  staging_user                    default
```

### Switch context

```shell
$ kubectl config use-context <CONTEXT>
```

### Switch namespace

```shell
$ kubectl config set-context --current --namespace <NAMESPACE>
```

## CronJob

### Run CronJob immediate

```shell   
kubectl create job test --from=cronjobs/cronjob123
```

## Events

### Get latest events

```shell
$ kubectl get events --sort-by='{.lastTimestamp}'
```

### Colorized event watching

```shell
$ kubectl get events --all-namespaces --watch \
    -o 'go-template={{.lastTimestamp}} _ {{.involvedObject.kind}} _ {{.message}} _ ({{.involvedObject.name}}){{"\n"}}' \
    | awk -F_ \
    -v black=$(tput setaf 0) \
    -v red=$(tput setaf 1) \
    -v green=$(tput setaf 2) \
    -v yellow=$(tput setaf 3) \
    -v blue=$(tput setaf 4) \
    -v magenta=$(tput setaf 5) \
    -v cyan=$(tput setaf 6) \
    -v white=$(tput setaf 7) \
    '{
        $1=blue $1
        $2=green $2
        $3=white $3
    }
    1'
```

## Secrets

### Show secret encoded

```shell
$ kubectl get secret -o yaml the-secret-we-want-to-see | yq '.data |= map_values(. | @base64d)'
```

## Image Pull Secrets

### Add default Image Pull Secret to Service-Account

```shell
$ kubectl patch serviceaccount <SERVICE_ACCOUNT_NAME> -p '{"imagePullSecrets": [{"name": "<SECRET_NAME"}]}'
```

## Nginx Ingress

### Domain Redirect

The redirect code 307 preserves the sending of payload on a POST request.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/permanent-redirect: https://target.domain.de$request_uri
    nginx.ingress.kubernetes.io/permanent-redirect-code: '307'
  name: keycloak-redirect
spec:
  ingressClassName: nginx
  rules:
    - host: domain.which.should.be.redirect.de
  tls:
    - hosts:
        - domain.which.should.be.redirect.de
      secretName: some-secret-tls
```

## Debugging

```shell
$ kubectl run --rm -it debugging --image alpine

$ kubectl run --rm -ti debugging --image alpine --overrides='
{
    "spec": {
        "containers": [
            {
                "stdin": true,
                "tty": true,
                "args": [ "/bin/sh" ],
                "name": "debugging",
                "image": "alpine",
                "volumeMounts": [
                    {
                        "mountPath": "/data",
                        "name": "data"
                    }
                ]
            }
        ],
        "volumes": [
            {
                "name": "data",
                "emptyDir": {}
            }
        ]
    }
}'
```

## JSON-Path Queries

### Show special Env-Value

Replace `<ENV_NAME>` with the real env name.

```shell
kubectl get pods -o=jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.spec.containers[0].env[?(@.name=="<ENV_NAME>")].value}{"\n"}{end}
```

## Versioning

### Fetching resource in specific version

```shell
kubectl get <RESOURCE_KIND>.<RESOURCE_VERSION>.<RESOURCE_GROUP>
```

Example:
```shell
kubectl get alertmanagerconfig.v1alpha1.monitoring.coreos.com
kubectl get alertmanagerconfig.v1beta1.monitoring.coreos.com
```