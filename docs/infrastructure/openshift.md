# OpenShift Tricks

## Login to the container registry of a Project/Namespace

The `IMAGE_STREAM_URL` needs to be taken from the "Public image repository" attribute of the ImageStream in OpenShift, but without the last part.

E.g. if the "Public image repository" of the ImageStream "sample-application" would be `default-route-openshift-image-registry.apps.my-cluster.com/ocp0000001/sample-application` you need to take simply `default-route-openshift-image-registry.apps.my-cluster.com/ocp0000001` for the `IMAGE_STREAM_URL`.

```shell
docker login -u <USERNAME> -e <EMAIL> -p $(oc whoami -t) <IMAGE_STREAM_URL>
```

