# Azure CLI Commands

## Azure Kubernetes Service

### Get credentials for kubectl

```shell
$ az aks get-credentials --name <CLUSTER-NAME> --resource-group <RESOURCE-GROUP> --context <CONTEXT-NAME-IN-KUBECTL>
```
