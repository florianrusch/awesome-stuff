# Container vs Serverless

> "Choose containers and container orchestrators when you need flexibility, or when you need to migrate legacy services. Choose serverless when you need speed of development, automatic scaling and significantly lowered runtime costs." serverless.com

## Containers

### Advantages

- Control and Flexibility
- Vendor-agnostic/Portability (Env and Cloud independent)

### Disadvantages

- Administrative work (e.g. apply security fixes for containers)
- Scaling is slower
- Running costs
- More manual intervention

## Serverless / FaaS

### Advantages

- Pay-per-execution
- Zero cost for idle time
- Auto-scaling
- Microservice nature —> Clear codebase separation
- Significantly reduced administration and maintenance burden

### Disadvantages

- "Black box" environment
- Vendor lock-in (Not Env and Cloud independent)
- Cold starts (Takes time until the Function/lambda will be provisioned)
- Complex apps can be hard to build (e.g. a lot of functions)
- No standardization (though the CNCF is working on this)
