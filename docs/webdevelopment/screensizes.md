# Screen sizes/resolutions

Quelle: <https://gs.statcounter.com/screen-resolution-stats/mobile/europe>

## Mobile

- 360x640 – 27.44%
- 375x667 – 11.64%
- 360x740 – 6.01%
- 360x720 – 5.4%
- 412x846 – 4.8%
- 360x780 – 4.17%

## Tablet

- 768x1024 – 60.88%
- 1280x800 – 7.62%
- 800x1280 – 4.74%
- 1024x1366 – 3.68%
- 834x1112 – 3.37%
- 1024x768 – 2.28%

## Desktop

- 1920x1080 – 24.36%
- 1366x768 – 19.64%
- 1536x864 – 7.48%
- 1440x900 – 7.29%
- 1600x900 – 4.66%
- 1280x1024 – 4.16%