# Node.js

## Find all `node_modules/` folders

Finds all `node_modules/` folders recursive in the current folder.

```shell
find . -name "node_modules" -type d -prune
```

## Remove all `node_modules/` folders

Removes all `node_modules/` folders recursive in the current folder.

```shell
find . -name "node_modules" -type d -prune -exec rm -rf '{}' +
```